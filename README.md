# Enter in Dorne (ftp)
Use auth oberynmartell:A_verySmallManCanCastAVeryLargeShad0w

Flag: fb8d98be1265dd88bac522e1b2182140

# Finding the password at format md5(md5($s).$p)

nobody:6000e084bf18c302eae4559d48cb520c$2hY68a

`hashcat -m 20 hashfile --show`

6000e084bf18c302eae4559d48cb520c:0cbb5be2c4504bed573802efbd909965:stark

# Decrypt the_wall.txt.nc
Use password stark from hashfile

mcrypt -d dorne/the_wall.txt.nc 

# The wall & The North (http)

## Winterfell 
Use /etc/hosts to access 7kingdoms as winterfell.7kingdoms.ctf

wget -rc http://jonsnow:Ha1lt0th3k1ng1nth3n0rth!!!@winterfell.7kingdoms.ctf/------W1nt3rf3ll------/

Flag: 639bae9ac6b3e1a84cebb7b403297b79


"Timef0rconqu3rs TeXT should be asked to enter into the Iron Islands fortress" - Theon Greyjoy

# Iron Islands (dns)

dig -tTXT  @10.224.97.127 Timef0rconqu3rs.7kingdoms.ctf

Timef0rconqu3rs.7kingdoms.ctf. 86400 IN	TXT	"You conquered Iron Islands kingdom flag: 5e93de3efa544e85dcd6311732d28f95. Now you should go to Stormlands at http://stormlands.7kingdoms.ctf:10000 . Enter using this user/pass combination: aryastark/N3ddl3_1s_a_g00d_sword#!"


Flag: 5e93de3efa544e85dcd6311732d28f95

# Stormlands

Access http://stormlands.7kingdoms.ctf:10000/file/ using a browser with Java enable (firefox ESR, for example).

Flag: 8fc42c6ddf9966db3b09e84365034357


Welcome to:
 _____ _                 _           _     
|   __| |_ ___ ___ _____| |___ ___ _| |___ 
|__   |  _| . |  _|     | | . |   | . |_ -|
|_____|_| |___|_| |_|_|_|_|__,|_|_|___|___|

Congratulations! you conquered Stormlands. This is your flag: 8fc42c6ddf9966db3b09e84365034357

Now prepare yourself for the next challenge!

The credentials to access to the Mountain and the Vale kingdom are:
user/pass: robinarryn/cr0wn_f0r_a_King-_
db: mountainandthevale

pgAdmin magic will not work. Command line should be used on that kingdom - Talisa Maegyr


